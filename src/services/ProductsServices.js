import axios from "axios";

const baseUrl = 'http://localhost:8080/api/v1/products';

export function getProducts() {
  return axios.get(baseUrl);
}

export function postProduct(product) {
  return axios.post(baseUrl, product);
}

export function putProduct(id, product) {
  return axios.put(baseUrl + `/${id}`, product);
}

export function deleteProduct(id) {
  return axios.delete(baseUrl + `/${id}`);
}